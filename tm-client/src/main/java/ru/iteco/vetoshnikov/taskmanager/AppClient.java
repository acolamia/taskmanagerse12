package ru.iteco.vetoshnikov.taskmanager;

import ru.iteco.vetoshnikov.taskmanager.bootstrap.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class AppClient {

    public static void main(@Nullable String[] args) {
        @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}