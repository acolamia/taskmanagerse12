
package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mergeProject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mergeProject"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="projectObject" type="{http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/}project" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mergeProject", propOrder = {
    "projectObject"
})
public class MergeProject {

    protected Project projectObject;

    /**
     * Gets the value of the projectObject property.
     * 
     * @return
     *     possible object is
     *     {@link Project }
     *     
     */
    public Project getProjectObject() {
        return projectObject;
    }

    /**
     * Sets the value of the projectObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Project }
     *     
     */
    public void setProjectObject(Project value) {
        this.projectObject = value;
    }

}
