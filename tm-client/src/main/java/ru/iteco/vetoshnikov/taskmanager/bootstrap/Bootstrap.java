package ru.iteco.vetoshnikov.taskmanager.bootstrap;

import org.reflections.Reflections;
import ru.iteco.vetoshnikov.taskmanager.api.IService;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import ru.iteco.vetoshnikov.taskmanager.endpoint.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.service.CommandService;
import ru.iteco.vetoshnikov.taskmanager.service.SessionStatusService;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator, IService {
    @NotNull
    private final SessionStatusService sessionStatusService = new SessionStatusService();
    @NotNull
    private final DomainEndpointService domainEndpointService = new DomainEndpointService();
    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();
    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();
    @NotNull
    private final SessionEndpointService sessionEndpointService=new SessionEndpointService();
    @NotNull
    private final CommandService commandService = new CommandService();
    @Nullable
    private final Scanner sc = new Scanner(System.in);
    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.iteco.vetoshnikov.taskmanager").getSubTypesOf(AbstractCommand.class);

    public void init() {
        try {
            registerCommandClasses(classes);
            System.out.println("Task manager");
            System.out.println("Введите help чтобы получить справку по командам ");
            while (true) {

    //                System.out.print("> ");
                @Nullable final String enterCommand = sc.nextLine();
                if (!commandService.getCommandMap().containsKey(enterCommand)) {
                    System.out.println("Такой команды не существует.");
                    continue;
                }
                if (commandService.commandMap.get(enterCommand).isSecure() && sessionStatusService.getSession() == null) {
                    System.out.println("Необходима авторизация.");
                    continue;
                }
                commandService.commandMap.get(enterCommand).execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerCommandClasses(@Nullable Set<Class<? extends AbstractCommand>> classes) throws Exception {
        if (classes == null) return;
        for (@Nullable Class getClass : classes) {
            if (getClass == null || !AbstractCommand.class.isAssignableFrom(getClass)) {
                continue;
            }
            @NotNull AbstractCommand command = (AbstractCommand) getClass.newInstance();
            command.setServiceLocator(this);
            command.setService(this);
            registerCommand(command);
        }
    }

    private void registerCommand(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandService.commandMap.put(command.command(), command);
    }

    @SneakyThrows
    @Override
    public Scanner getScanner() {
        return sc;
    }
}
