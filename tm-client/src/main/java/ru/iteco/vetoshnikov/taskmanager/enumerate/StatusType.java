package ru.iteco.vetoshnikov.taskmanager.enumerate;

public enum  StatusType {
    PLANNED("Запланировано"),
    INPROGRESS("В процессе"),
    COMPLETE("Выполнено");

    private final String displayName;

    StatusType(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
