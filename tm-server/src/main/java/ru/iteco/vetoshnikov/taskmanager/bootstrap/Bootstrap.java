package ru.iteco.vetoshnikov.taskmanager.bootstrap;

import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.endpoint.*;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.repository.ProjectRepository;
import ru.iteco.vetoshnikov.taskmanager.repository.SessionRepository;
import ru.iteco.vetoshnikov.taskmanager.repository.TaskRepository;
import ru.iteco.vetoshnikov.taskmanager.repository.UserRepository;
import ru.iteco.vetoshnikov.taskmanager.service.*;

import javax.xml.ws.Endpoint;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {
    @NotNull
    private final SessionRepository sessionRepository = new SessionRepository();
    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final TaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final UserRepository userRepository = new UserRepository();
    @NotNull
    private final SessionService sessionService = new SessionService(this,sessionRepository);
    @NotNull
    private final ProjectService projectService = new ProjectService(projectRepository);
    @NotNull
    private final TaskService taskService = new TaskService(taskRepository);
    @NotNull
    private final UserService userService = new UserService(userRepository);
//    @Nullable
//    public static Connection connection;


    public void init() {
        try {
//            connection = SqlConnect.getConnect();
            setEndpoint();
            addUserAndAdminUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setEndpoint() {
        DomainEndpoint domainEndpoint = new DomainEndpoint();
        domainEndpoint.setServiceLocator(this);
        Endpoint.publish("http://localhost:8080/DomainWebService?wsdl", domainEndpoint);

        ProjectEndpoint projectEndpoint = new ProjectEndpoint();
        projectEndpoint.setServiceLocator(this);
        Endpoint.publish("http://localhost:8080/ProjectWebService?wsdl", projectEndpoint);

        TaskEndpoint taskEndpoint = new TaskEndpoint();
        taskEndpoint.setServiceLocator(this);
        Endpoint.publish("http://localhost:8080/TaskWebService?wsdl", taskEndpoint);

        UserEndpoint userEndpoint = new UserEndpoint();
        userEndpoint.setServiceLocator(this);
        Endpoint.publish("http://localhost:8080/UserWebService?wsdl", userEndpoint);

        SessionEndpoint sessionEndpoint = new SessionEndpoint();
        sessionEndpoint.setServiceLocator(this);
        Endpoint.publish("http://localhost:8080/SessionWebService?wsdl", sessionEndpoint);
    }

    private void addUserAndAdminUser() {
        User user = new User("user", "user", RoleType.USER.getDisplayName());
        user.setName("user");
        userService.createUser(user);
        User admin = new User("admin", "admin", RoleType.ADMINISTRATOR.getDisplayName());
        admin.setName("admin");
        userService.createUser(admin);
    }
}
