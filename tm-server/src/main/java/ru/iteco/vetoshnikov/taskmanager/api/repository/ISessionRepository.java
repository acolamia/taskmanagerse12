package ru.iteco.vetoshnikov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;

import java.sql.Connection;

public interface ISessionRepository {
    Session createSession(@NotNull Connection connection, @NotNull String userId);
}
