package ru.iteco.vetoshnikov.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.repository.IUserRepository;
import ru.iteco.vetoshnikov.taskmanager.constant.FieldConst;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class UserRepository implements IUserRepository {
    @Override
    @SneakyThrows
    public void merge(@NotNull final Connection connection, @NotNull final User user) {
        @NotNull String query = "UPDATE app_user SET " +
                FieldConst.LOGIN + "=?, " +
                FieldConst.PASSWORD + "=?, " +
                FieldConst.ROLE_TYPE + "=?, " +
                FieldConst.NAME + "=?, " +
                FieldConst.DESCRIPTION + "=?, " +
                FieldConst.BEGIN_DATE + "=?, " +
                FieldConst.END_DATE + "=?" +
                FieldConst.CREATE_DATE + "=?" +
                " WHERE " + FieldConst.ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPassword());
        statement.setString(3, user.getRole());
        statement.setString(4, user.getName());
        statement.setString(5, user.getDescription());
        statement.setObject(6, user.getBeginDate());
        statement.setObject(7, user.getEndDate());
        statement.setObject(8, user.getCreateDate());
        statement.setString(9, user.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final Connection connection, @NotNull final String key) {
        @NotNull final String query = "DELETE FROM app_user WHERE " +
                FieldConst.ID + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, key);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final Connection connection) {
        @NotNull final String query = "DELETE FROM app_user";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public User findOne(@NotNull final Connection connection, @NotNull final String key) {
        @NotNull final String query = "SELECT * FROM app_user WHERE " +
                FieldConst.ID + " =?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, key);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        User user = fetch(resultSet);
        if (user != null) return user;
        statement.close();
        return null;
    }

    @Override
    @SneakyThrows
    public List<User> findAll(@NotNull final Connection connection) {
        String query = "SELECT * FROM app_user";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<User> userList = new ArrayList<>();
        while (resultSet.next()) {
            userList.add(fetch(resultSet));
        }
        statement.close();
        return userList;
    }

    @Override
    @SneakyThrows
    public void persist(@NotNull final Connection connection, @NotNull final User user) {
        @NotNull final String queryCheck = "SELECT * FROM app_user WHERE " +
                FieldConst.LOGIN + " =?";
        @NotNull final PreparedStatement statementCheck = connection.prepareStatement(queryCheck);
        statementCheck.setString(1, user.getLogin());
        @NotNull final ResultSet resultSet = statementCheck.executeQuery();
        if (resultSet.next()) {
            statementCheck.close();
            return;
        }

        statementCheck.close();
        @NotNull final String query = "INSERT INTO app_user (" +
                FieldConst.ID + ", " +
                FieldConst.LOGIN + ", " +
                FieldConst.PASSWORD + ", " +
                FieldConst.ROLE_TYPE + ", " +
                FieldConst.NAME + ", " +
                FieldConst.DESCRIPTION + ", " +
                FieldConst.BEGIN_DATE + ", " +
                FieldConst.END_DATE + "," +
                FieldConst.CREATE_DATE + " " +
                ") VALUES (?,?,?,?,?,?,?,?,?)";

        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPassword());
        statement.setString(4, user.getRole());
        statement.setString(5, user.getName());
        statement.setString(6, user.getDescription());
        statement.setObject(7, user.getBeginDate());
        statement.setObject(8, user.getEndDate());
        statement.setObject(9, user.getCreateDate());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void load(@NotNull final Connection connection, @NotNull final Domain domain) {
        List<User> userList = domain.getUserList();

        @NotNull final String query = "INSERT INTO app_user (" +
                FieldConst.ID + ", " +
                FieldConst.LOGIN + ", " +
                FieldConst.PASSWORD + ", " +
                FieldConst.ROLE_TYPE + ", " +
                FieldConst.NAME + ", " +
                FieldConst.DESCRIPTION + ", " +
                FieldConst.BEGIN_DATE + ", " +
                FieldConst.END_DATE + "," +
                FieldConst.CREATE_DATE +
                ") VALUES (?,?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        for (@Nullable final User user : userList) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getRole());
            statement.setString(5, user.getName());
            statement.setString(6, user.getDescription());
            statement.setObject(7, user.getBeginDate());
            statement.setObject(8, user.getEndDate());
            statement.setObject(9, user.getCreateDate());
            statement.addBatch();
        }
        statement.executeBatch();
        statement.close();
        connection.close();
    }

    @Override
    @SneakyThrows
    public String getIdUser(@NotNull final Connection connection, @NotNull final String login) {
        @NotNull final String query = "SELECT * FROM app_user WHERE " +
                FieldConst.LOGIN + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final User user = fetch(resultSet);
        if (user == null) return null;
        statement.close();
        return user.getId();
    }

    @Nullable
    @SneakyThrows
    private User fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString(FieldConst.ID));
        user.setLogin(row.getString(FieldConst.LOGIN));
        user.setPassword(row.getString(FieldConst.PASSWORD));
        user.setName(row.getString(FieldConst.NAME));
        user.setDescription(row.getString(FieldConst.DESCRIPTION));
        user.setBeginDate(row.getDate(FieldConst.BEGIN_DATE));
        user.setEndDate(row.getDate(FieldConst.END_DATE));
        user.setCreateDate(row.getDate(FieldConst.CREATE_DATE));
        @Nullable final String role = row.getString(FieldConst.ROLE_TYPE);
        if (role != null) {
            if ("Пользователь".equals(role)) {
                user.setRole("Пользователь");
            }
            if ("Администратор".equals(role)) {
                user.setRole("Администратор");
            }
        }
        return user;
    }

}
