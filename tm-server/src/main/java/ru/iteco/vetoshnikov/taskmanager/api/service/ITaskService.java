package ru.iteco.vetoshnikov.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;

import java.util.List;

public interface ITaskService {
    void createTask(@Nullable Task task);

    void merge(@Nullable Task task);

    List<Task> merge(@Nullable List<Task> taskList);

    void remove(@Nullable String userId, String projectId, String taskName);

    void removeAllByProject(@Nullable String userId, String projectId);

    void clear();

    Task findOne(@Nullable String userId, String projectId, String taskName);

    @Nullable List<Task> findAll();

    @Nullable List<Task> findAllByProject(String userId, String projectId);

    void load(@Nullable Domain domain);

    String getIdTask(@Nullable String userId, @Nullable String projectId, @Nullable String taskName);
}
