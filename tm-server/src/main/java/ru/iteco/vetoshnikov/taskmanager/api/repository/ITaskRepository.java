package ru.iteco.vetoshnikov.taskmanager.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;

import java.sql.Connection;
import java.util.List;

public interface ITaskRepository {
    void merge(@NotNull Connection connection, @NotNull Task task);

    void remove(@NotNull Connection connection, @Nullable String userId, String projectId, String taskName);

    void clear(@NotNull Connection connection);

    Task findOne(@NotNull Connection connection, @NotNull String userId, String projectId, String taskName);

    List<Task> findAll(@NotNull Connection connection);

    List<Task> findAllByProject(@NotNull Connection connection, String userId, String projectId);

    void persist(@NotNull Connection connection, @NotNull Task task);

    void removeAllByProject(@NotNull Connection connection, @Nullable String userId, String projectId);

    void load(@NotNull Connection connection, @NotNull Domain domain);

    String getIdTask(@NotNull Connection connection, @NotNull String userId, @NotNull String projectId, @NotNull String name);
}
