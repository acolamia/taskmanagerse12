package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createTaskTask(
            @WebParam(name = "taskObject") @Nullable final Task taskObject
    ) {
        serviceLocator.getTaskService().createTask(taskObject);
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "taskObject") @Nullable final Task taskObject
    ) {
        serviceLocator.getTaskService().merge(taskObject);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "userId") @Nullable final String  userId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) {
        serviceLocator.getTaskService().remove(userId, projectId, taskName);
    }

    @Override
    @WebMethod
    public void clearTask(
    ) {
        serviceLocator.getTaskService().clear();
    }

    @Override
    @WebMethod
    public Task findOneTask(
            @WebParam(name = "userId") @Nullable final String  userId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) {
        return serviceLocator.getTaskService().findOne(userId, projectId, taskName);
    }

    @Override
    @WebMethod
    public List<Task> findAllTask(
            ) {
        return serviceLocator.getTaskService().findAll();
    }

    @Override
    @WebMethod
    public List<Task> findAllByProjectTask(
            @WebParam(name = "userId") @Nullable final String  userId,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) {
        return serviceLocator.getTaskService().findAllByProject(userId, projectId);
    }

    @Override
    @WebMethod
    public void removeAllByProjectTask(
            @WebParam(name = "userId") @Nullable final String  userId,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) {
        serviceLocator.getTaskService().removeAllByProject(userId, projectId);
    }

    @Override
    @WebMethod
    public void loadTask(
            @WebParam(name = "domainObject") @Nullable final Domain domainObject
    ) {
        serviceLocator.getTaskService().load(domainObject);
    }

    @Override
    @WebMethod
    public String getIdTaskTask(
            @WebParam(name = "userId") @Nullable final String  userId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) {
        return serviceLocator.getTaskService().getIdTask(userId, projectId, taskName);
    }
}
