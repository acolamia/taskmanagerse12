package ru.iteco.vetoshnikov.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.repository.ITaskRepository;
import ru.iteco.vetoshnikov.taskmanager.constant.FieldConst;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

@NoArgsConstructor
public final class TaskRepository implements ITaskRepository {
    @Override
    @SneakyThrows
    public void merge(@NotNull final Connection connection, @NotNull final Task task) {
        @NotNull String query = "UPDATE app_task SET " +
                FieldConst.PROJECT_ID + "=?, " +
                FieldConst.USER_ID + "=?, " +
                FieldConst.STATUS_TYPE + "=?, " +
                FieldConst.NAME + "=?, " +
                FieldConst.DESCRIPTION + "=?, " +
                FieldConst.BEGIN_DATE + "=?, " +
                FieldConst.END_DATE + "=?, " +
                FieldConst.CREATE_DATE + "=? " +
                "WHERE " + FieldConst.ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getProjectId());
        statement.setString(2, task.getUserId());
        statement.setString(3, task.getStatusType());
        statement.setString(4, task.getName());
        statement.setString(5, task.getDescription());
        statement.setObject(6, task.getBeginDate());
        statement.setObject(7, task.getEndDate());
        statement.setObject(8, task.getCreateDate());
        statement.setString(9, task.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final Connection connection, @Nullable final String userId, String projectId, String taskName) {
        @NotNull final String query = "DELETE FROM app_task WHERE " +
                FieldConst.USER_ID + " =?" + " AND " +
                FieldConst.PROJECT_ID + "=?" + " AND " +
                FieldConst.NAME + " =?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.setString(3, taskName);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final Connection connection) {
        @NotNull final String query = "DELETE FROM app_task";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public Task findOne(@NotNull final Connection connection, @NotNull final String userId, String projectId, String taskName) {
        @NotNull final String query = "SELECT * FROM app_task WHERE " +
                FieldConst.USER_ID + " =? AND " +
                FieldConst.PROJECT_ID + " =? AND " +
                FieldConst.NAME + " =?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.setString(3, taskName);
        ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        Task task = fetch(resultSet);
        if (task != null) return task;
        statement.close();
        return null;
    }

    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final Connection connection) {
        @NotNull final String query = "SELECT * FROM app_task";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> taskList = new ArrayList<>();
        while (resultSet.next()) {
            taskList.add(fetch(resultSet));
        }
        statement.close();
        return taskList;
    }

    @Override
    @SneakyThrows
    public List<Task> findAllByProject(@NotNull final Connection connection, @NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "SELECT * FROM app_task WHERE " +
                FieldConst.USER_ID + "=?" + " AND " +
                FieldConst.PROJECT_ID + "=?";
        System.out.println(query);
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(fetch(resultSet));
        }
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void persist(@NotNull final Connection connection, @NotNull final Task task) {
        @NotNull final String query = "INSERT INTO app_task (" +
                FieldConst.ID + ", " +
                FieldConst.PROJECT_ID + ", " +
                FieldConst.USER_ID + ", " +
                FieldConst.STATUS_TYPE + ", " +
                FieldConst.NAME + ", " +
                FieldConst.DESCRIPTION + ", " +
                FieldConst.BEGIN_DATE + ", " +
                FieldConst.END_DATE + ", " +
                FieldConst.CREATE_DATE +
                ") VALUES (?,?,?,?,?,?,?,?,?)";

        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getProjectId());
        statement.setString(3, task.getUserId());
        statement.setString(4, task.getStatusType());
        statement.setString(5, task.getName());
        statement.setString(6, task.getDescription());
        statement.setObject(7, task.getBeginDate());
        statement.setObject(8, task.getEndDate());
        statement.setObject(9, task.getCreateDate());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeAllByProject(@NotNull final Connection connection, @Nullable final String userId, String projectId) {
        @NotNull final String query = "DELETE FROM app_task WHERE " +
                FieldConst.USER_ID + " =?" + " AND " +
                FieldConst.PROJECT_ID + " =?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void load(@NotNull final Connection connection, @NotNull final Domain domain) {
        List<Task> taskList = domain.getTaskList();
        @NotNull final String query = "INSERT INTO app_task (" +
                FieldConst.ID + ", " +
                FieldConst.PROJECT_ID + ", " +
                FieldConst.USER_ID + ", " +
                FieldConst.STATUS_TYPE + ", " +
                FieldConst.NAME + ", " +
                FieldConst.DESCRIPTION + ", " +
                FieldConst.BEGIN_DATE + ", " +
                FieldConst.END_DATE + ", " +
                FieldConst.CREATE_DATE +
                ") VALUES (?,?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        for (@Nullable final Task task : taskList) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getProjectId());
            statement.setString(3, task.getUserId());
            statement.setString(4, task.getStatusType());
            statement.setString(5, task.getName());
            statement.setString(6, task.getDescription());
            statement.setObject(7, task.getBeginDate());
            statement.setObject(8, task.getEndDate());
            statement.setObject(9, task.getCreateDate());
            statement.addBatch();
        }
        statement.executeBatch();
        statement.close();
        connection.close();
    }

    @Override
    @SneakyThrows
    public String getIdTask(@NotNull final Connection connection, @NotNull final String userId, @NotNull final String projectId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM app_project WHERE " +
                FieldConst.USER_ID + " =?" + " AND " +
                FieldConst.PROJECT_ID + " = ?" + " AND " +
                FieldConst.NAME + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.setString(3, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Task task = fetch(resultSet);
        if (task == null) return null;
        statement.close();
        return task.getId();
    }

    @Nullable
    @SneakyThrows
    private Task fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(FieldConst.ID));
        task.setProjectId(row.getString(FieldConst.PROJECT_ID));
        task.setUserId(row.getString(FieldConst.USER_ID));
        task.setName(row.getString(FieldConst.NAME));
        task.setDescription(row.getString(FieldConst.DESCRIPTION));
        task.setBeginDate(row.getDate(FieldConst.BEGIN_DATE));
        task.setEndDate(row.getDate(FieldConst.END_DATE));
        task.setCreateDate(row.getDate(FieldConst.CREATE_DATE));

        @Nullable final String status = row.getString(FieldConst.STATUS_TYPE);
        if (status != null) {
            switch (status) {
                case ("Запланировано"):
                    task.setStatusType(StatusType.PLANNED.getDisplayName());
                    break;
                case ("В процессе"):
                    task.setStatusType(StatusType.INPROGRESS.getDisplayName());
                    break;
                case ("Выполнено"):
                    task.setStatusType(StatusType.COMPLETE.getDisplayName());
                    break;
            }
        }
        return task;
    }
}
