package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.repository.ProjectRepository;
import lombok.*;
import ru.iteco.vetoshnikov.taskmanager.util.SqlConnect;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class ProjectService extends AbstractService implements IProjectService {
    private final ProjectRepository projectRepository;

    @Override
    public void createProject(@Nullable final Project project) {
        if (project == null) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            projectRepository.persist(connection, project);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            projectRepository.merge(connection, project);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Project> merge(@Nullable final List<Project> projectList) {
        if (projectList == null || projectList.isEmpty()) return null;
        for (@Nullable final Project project : projectList) {
            if (project == null) continue;
            Connection connection = null;
            try {
                connection = SqlConnect.getConnect();
                if (connection == null) return null;
                projectRepository.merge(connection, project);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return projectList;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty() || projectName == null || projectName.isEmpty()) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            projectRepository.remove(connection, userId, projectName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeAllByUser(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            projectRepository.removeAllByUser(connection, userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void clear() {
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            projectRepository.clear(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty() || projectName == null || projectName.isEmpty()) return null;
        Connection connection = null;
        @Nullable Project project = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            project = projectRepository.findOne(connection, userId, projectName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return project;
    }

    @Override
    public @Nullable
    final List<Project> findAll() {
        Connection connection = null;
        @Nullable List<Project> projectList = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            projectList = projectRepository.findAll(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projectList;
    }

    @Override
    public @Nullable
    final List<Project> findAllByUser(@Nullable final String userId) {
        Connection connection = null;
        @Nullable List<Project> projectList = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            projectList = projectRepository.findAllByUser(connection, userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projectList;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            projectRepository.clear(connection);
            projectRepository.load(connection, domain);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getIdProject(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty() || name == null || name.isEmpty()) return null;
        Connection connection = null;
        @Nullable String projectId = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            projectId = projectRepository.getIdProject(connection, userId, name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projectId;
    }
}
