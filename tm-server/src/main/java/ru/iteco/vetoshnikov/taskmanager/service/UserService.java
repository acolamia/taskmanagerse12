package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.repository.UserRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.iteco.vetoshnikov.taskmanager.util.SqlConnect;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class UserService extends AbstractService implements IUserService {
    private final UserRepository userRepository;

    @Override
    public void createUser(@Nullable final User user) {
        if (user == null) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            userRepository.persist(connection, user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            userRepository.merge(connection, user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> merge(@Nullable final List<User> userList) {
        if (userList == null || userList.isEmpty()) return null;
        for (@Nullable final User user : userList) {
            if (user == null) continue;
            Connection connection = null;
            try {
                connection = SqlConnect.getConnect();
                if (connection == null) return null;
                userRepository.merge(connection, user);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return userList;
    }

    @Override
    public void remove(@Nullable final String key) {
        if (key == null || key.isEmpty()) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            userRepository.remove(connection, key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User findOne(@Nullable final String key) {
        if (key == null || key.isEmpty()) return null;
        Connection connection = null;
        User user = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            user = userRepository.findOne(connection, key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public @Nullable
    final List<User> findAll() {
        Connection connection = null;
        List<User> userList = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            userList = userRepository.findAll(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            userRepository.clear(connection);
            userRepository.load(connection, domain);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getIdUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        Connection connection = null;
        String userId = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            userId = userRepository.getIdUser(connection, login);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userId;
    }

    @Override
    public void clear() {
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            userRepository.clear(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
