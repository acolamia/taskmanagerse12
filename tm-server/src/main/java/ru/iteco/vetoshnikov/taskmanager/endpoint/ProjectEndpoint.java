package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createProjectProject(
            @WebParam(name = "projectObject") @Nullable final Project projectObject
    ) {
        serviceLocator.getProjectService().createProject(projectObject);
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "projectObject") @Nullable final Project projectObject
    ) {
        serviceLocator.getProjectService().merge(projectObject);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "userId") @Nullable final String  userId,
            @WebParam(name = "projectName") @Nullable final String projectName
    ) {
        serviceLocator.getProjectService().remove(userId, projectName);
    }

    @Override
    @WebMethod
    public void clearProject(
    ) {
        serviceLocator.getProjectService().clear();
    }

    @Override
    @WebMethod
    public Project findOneProject(
            @WebParam(name = "userId") @Nullable final String  userId,
            @WebParam(name = "projectName") @Nullable final String projectName
    ) {
        return serviceLocator.getProjectService().findOne(userId, projectName);
    }

    @Override
    @WebMethod
    public List<Project> findAllProject(
    ) {
        return serviceLocator.getProjectService().findAll();
    }

    @Override
    @WebMethod
    public List<Project> findAllByUserProject(
            @WebParam(name = "userId") @Nullable final String  userId
    ) {
        return serviceLocator.getProjectService().findAllByUser(userId);
    }

    @Override
    @WebMethod
    public void removeAllByUserProject(
            @WebParam(name = "userId") @Nullable final String  userId
    ) {
        serviceLocator.getProjectService().removeAllByUser(userId);
    }

    @Override
    @WebMethod
    public void loadProject(
            @WebParam(name = "domainObject") @Nullable final Domain domainObject
    ) {
        serviceLocator.getProjectService().load(domainObject);
    }

    @Override
    @WebMethod
    public String getIdProjectProject(
            @WebParam(name = "userId") @Nullable final String  userId,
            @WebParam(name = "projectName") @Nullable final String projectName
    ) {
        return serviceLocator.getProjectService().getIdProject(userId, projectName);
    }

}