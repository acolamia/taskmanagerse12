package ru.iteco.vetoshnikov.taskmanager.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;

import java.sql.Connection;
import java.util.List;

public interface IProjectRepository {
    void merge(@NotNull Connection connection, @NotNull Project project);

    void remove(@NotNull Connection connection, @Nullable String userId, String projectName);

    void clear(@NotNull Connection connection);

    Project findOne(@NotNull Connection connection, @Nullable String userId, String projectName);

    List<Project> findAll(@NotNull Connection connection);

    List<Project> findAllByUser(@NotNull Connection connection, String userId);

    void persist(@NotNull Connection connection, @NotNull Project project);

    void removeAllByUser(@NotNull Connection connection, @NotNull String userId);

    void load(@NotNull Connection connection, @NotNull Domain domain);

    String getIdProject(@NotNull Connection connection, @NotNull String userId, @NotNull String projectName);
}
