package ru.iteco.vetoshnikov.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.repository.IProjectRepository;
import ru.iteco.vetoshnikov.taskmanager.constant.FieldConst;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;

import java.sql.*;
import java.util.*;

@NoArgsConstructor
public final class ProjectRepository implements IProjectRepository {
    @Override
    @SneakyThrows
    public void merge(@NotNull final Connection connection, @NotNull final Project project) {
        @NotNull String query = "UPDATE app_project SET " +
                FieldConst.USER_ID + "=?," +
                FieldConst.STATUS_TYPE + "=?," +
                FieldConst.NAME + "=?," +
                FieldConst.DESCRIPTION + "=?," +
                FieldConst.BEGIN_DATE + "=?," +
                FieldConst.END_DATE + "=?," +
                FieldConst.CREATE_DATE + "=?" +
                " WHERE " + FieldConst.ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getUserId());
        statement.setString(2, project.getStatusType());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDescription());
        statement.setObject(5, project.getBeginDate());
        statement.setObject(6, project.getEndDate());
        statement.setObject(7, project.getCreateDate());
        statement.setString(8, project.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final Connection connection, @NotNull final String userId, @NotNull final String projectName) {
        @NotNull final String query = "DELETE FROM app_project WHERE " +
                FieldConst.USER_ID + "=? AND " +
                FieldConst.NAME + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectName);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final Connection connection) {
        @NotNull final String query = "DELETE FROM app_project";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public Project findOne(@NotNull final Connection connection, @Nullable final String userId, @Nullable final String projectName) {
        @NotNull final String query = "SELECT * FROM app_project WHERE " +
                FieldConst.USER_ID + "=?" + " AND " +
                FieldConst.NAME + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectName);
        ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        Project project = fetch(resultSet);
        if (project != null) return project;
        statement.close();
        return null;
    }

    @Override
    @SneakyThrows
    public List<Project> findAll(@NotNull final Connection connection) {
        @NotNull final String query = "SELECT * FROM app_project";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> projectList = new ArrayList<>();
        while (resultSet.next()) {
            projectList.add(fetch(resultSet));
        }
        statement.close();
        return projectList;
    }

    @Override
    @SneakyThrows
    public List<Project> findAllByUser(@NotNull final Connection connection, @NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM app_project WHERE " +
                FieldConst.USER_ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return null;
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(fetch(resultSet));
        }
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void persist(@NotNull final Connection connection, @NotNull final Project project) {
        @NotNull final String query = "INSERT INTO app_project (" +
                FieldConst.ID + ", " +
                FieldConst.USER_ID + ", " +
                FieldConst.STATUS_TYPE + ", " +
                FieldConst.NAME + ", " +
                FieldConst.DESCRIPTION + ", " +
                FieldConst.BEGIN_DATE + ", " +
                FieldConst.END_DATE + ", " +
                FieldConst.CREATE_DATE +
                ") VALUES (?,?,?,?,?,?,?,?)";

        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getStatusType());
        statement.setString(4, project.getName());
        statement.setString(5, project.getDescription());
        statement.setObject(6, project.getBeginDate());
        statement.setObject(7, project.getEndDate());
        statement.setObject(8, project.getCreateDate());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeAllByUser(@NotNull final Connection connection, @NotNull final String userId) {
        @NotNull final String query = "DELETE FROM app_project WHERE " +
                FieldConst.USER_ID + "=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void load(@NotNull final Connection connection, @NotNull final Domain domain) {
        List<Project> projectList = domain.getProjectList();
        @NotNull final String query = "INSERT INTO app_project (" +
                FieldConst.ID + ", " +
                FieldConst.USER_ID + ", " +
                FieldConst.STATUS_TYPE + ", " +
                FieldConst.NAME + ", " +
                FieldConst.DESCRIPTION + ", " +
                FieldConst.BEGIN_DATE + ", " +
                FieldConst.END_DATE + ", " +
                FieldConst.CREATE_DATE +
                ") VALUES (?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        for (@Nullable final Project project : projectList) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getUserId());
            statement.setString(3, project.getStatusType());
            statement.setString(4, project.getName());
            statement.setString(5, project.getDescription());
            statement.setObject(6, project.getBeginDate());
            statement.setObject(7, project.getEndDate());
            statement.setObject(8, project.getCreateDate());
            statement.addBatch();
        }
        statement.executeBatch();
        statement.close();
        connection.close();
    }

    @Override
    @SneakyThrows
    public String getIdProject(@NotNull final Connection connection, @NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM app_project WHERE " +
                FieldConst.USER_ID + "=?" + " AND " +
                FieldConst.NAME + "=?";

        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Project project = fetch(resultSet);
        if (project == null) return null;
        statement.close();
        return project.getId();
    }

    @Nullable
    @SneakyThrows
    private Project fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(FieldConst.ID));
        project.setUserId(row.getString(FieldConst.USER_ID));
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setBeginDate(row.getDate(FieldConst.BEGIN_DATE));
        project.setEndDate(row.getDate(FieldConst.END_DATE));
        project.setCreateDate(row.getDate(FieldConst.CREATE_DATE));
        @Nullable final String status = row.getString(FieldConst.STATUS_TYPE);
        if (status != null) {
            switch (status) {
                case ("Запланировано"):
                    project.setStatusType(StatusType.PLANNED.getDisplayName());
                    break;
                case ("В процессе"):
                    project.setStatusType(StatusType.INPROGRESS.getDisplayName());
                    break;
                case ("Выполнено"):
                    project.setStatusType(StatusType.COMPLETE.getDisplayName());
                    break;
            }
        }
        return project;
    }
}
