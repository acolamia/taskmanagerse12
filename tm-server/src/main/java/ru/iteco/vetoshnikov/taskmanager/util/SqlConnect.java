package ru.iteco.vetoshnikov.taskmanager.util;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public final class SqlConnect {
    public static Connection getConnect() throws Exception {
        Properties props = new Properties();
        FileInputStream in = new FileInputStream(System.getProperty("user.dir") + "/tm-server/mysql.properties");
        props.load(in);
        in.close();

        String url = props.getProperty("db.host");
        String username = props.getProperty("db.login");
        String password = props.getProperty("db.password");

        return DriverManager.getConnection(url, username, password);
    }
}
