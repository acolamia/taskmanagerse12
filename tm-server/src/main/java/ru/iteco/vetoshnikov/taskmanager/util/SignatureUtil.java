package ru.iteco.vetoshnikov.taskmanager.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;

public final class SignatureUtil {
    private static final String salt = "vetoshnikov";
    private static final Integer cycle = 111;

    @Nullable
    public static String sign(@Nullable final Object value) {
        try {
            @NotNull final ObjectMapper objectMapper =
                    new ObjectMapper();
            @NotNull final String json =
                    objectMapper.writeValueAsString(value);
            return sign(json);
        } catch (
                final JsonProcessingException e) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public static String sign(@Nullable final String value) {
        if (value == null || salt == null || cycle == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.getHash(salt + result + salt);
        }
        return result;
    }

    public static Session check(@Nullable final Session session) {
        if (session == null) return null;
        String signature = session.getSignature();
        session.setSignature(null);
        session.setSignature(sign(session));
        if (!session.getSignature().equals(signature)) return null;
        return session;
    }
}
