package ru.iteco.vetoshnikov.taskmanager.repository;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.repository.ISessionRepository;
import ru.iteco.vetoshnikov.taskmanager.constant.FieldConst;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.util.SignatureUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;

@NoArgsConstructor
public class SessionRepository implements ISessionRepository {
    @Override
    @SneakyThrows
    public Session createSession(@NotNull final Connection connection, @NotNull final String userId) {
        @NotNull final Session session = new Session();
        session.setUserId(userId);
        @Nullable final String signature = SignatureUtil.sign(session);
        if (signature == null) return null;
        session.setSignature(signature);
        @NotNull final String query = "INSERT INTO app_session (" +
                FieldConst.ID + ", " +
                FieldConst.TIMESTAMP + "," +
                FieldConst.USER_ID + ", " +
                FieldConst.SIGNATURE +
                ") VALUES (?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setLong(2, session.getTimestamp());
        statement.setString(3, userId);
        statement.setString(4, signature);
        statement.executeUpdate();
        statement.close();
        return session;
    }
//
//    @Nullable
//    @SneakyThrows
//    private User fetch(@Nullable final ResultSet row) {
//        if (row == null) return null;
//        @NotNull final User user = new User();
//        user.setId(row.getString(FieldConst.ID));
//        user.setLogin(row.getString(FieldConst.TIMESTAMP));
//        user.setPassword(row.getString(FieldConst.USER_ID));
//        user.setName(row.getString(FieldConst.SIGNATURE));
//        return user;
//    }
}
