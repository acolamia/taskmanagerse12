package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.repository.TaskRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.iteco.vetoshnikov.taskmanager.util.SqlConnect;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {
    private final TaskRepository taskRepository;

    @Override
    public void createTask(@Nullable final Task task) {
        if (task == null || task.getProjectId() == null || task.getProjectId().isEmpty()) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            taskRepository.persist(connection, task);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            taskRepository.merge(connection, task);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Task> merge(@Nullable final List<Task> taskList) {
        if (taskList == null || taskList.isEmpty()) return null;
        for (@Nullable final Task task : taskList) {
            if (task == null) continue;
            Connection connection = null;
            try {
                connection = SqlConnect.getConnect();
                if (connection == null) return null;
                taskRepository.merge(connection, task);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return taskList;
    }

    @Override
    public void remove(@Nullable final String userId, String projectId, String taskName) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        if (taskName == null || taskName.isEmpty()) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            taskRepository.remove(connection, userId, projectId, taskName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeAllByProject(@Nullable final String userId, String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            taskRepository.removeAllByProject(connection, userId, projectId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void clear() {
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return;
            taskRepository.clear(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Task findOne(@Nullable final String userId, String projectId, String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        Connection connection = null;
        Task task = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            task = taskRepository.findOne(connection, userId, projectId, taskName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return task;
    }

    @Override
    public @Nullable
    final List<Task> findAll() {
        Connection connection = null;
        List<Task> taskList = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            taskList = taskRepository.findAll(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return taskList;
    }

    @Override
    public @Nullable
    final List<Task> findAllByProject(String userId, String projectId) {
        Connection connection = null;
        List<Task> taskList = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            taskList = taskRepository.findAllByProject(connection, userId, projectId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return taskList;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        Connection connection = null;
        try {
            connection = SqlConnect.getConnect();
            if (domain == null) return;
            if (connection == null) return;
            taskRepository.clear(connection);
            taskRepository.load(connection, domain);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getIdTask(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        Connection connection = null;
        String taskId = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            taskId = taskRepository.getIdTask(connection, userId, projectId, taskName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return taskId;
    }
}