package ru.iteco.vetoshnikov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

import java.sql.Connection;
import java.util.List;

public interface IUserRepository {
    void merge(@NotNull Connection connection, @NotNull User user);

    void remove(@NotNull Connection connection, @NotNull String userId);

    void clear(@NotNull Connection connection);

    User findOne(@NotNull Connection connection, @NotNull String name);

    List<User> findAll(@NotNull Connection connection);

    void persist(@NotNull Connection connection, @NotNull User user);

    void load(@NotNull Connection connection, @NotNull Domain domain);

    String getIdUser(@NotNull Connection connection, @NotNull String login);
}
