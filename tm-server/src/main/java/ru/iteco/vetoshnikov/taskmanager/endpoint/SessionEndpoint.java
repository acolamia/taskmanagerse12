package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public Session getSession(
            @WebParam(name = "userLogin") @Nullable final String userLogin,
            @WebParam(name = "userPass") @Nullable final String userPass) {
        @Nullable final User user = serviceLocator.getSessionService().checkPassword(userLogin, userPass);
        if (user == null) {
            return null;
        }
        @NotNull final String userId = user.getId();
        return serviceLocator.getSessionService().createSession(userId);
    }
}
