package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.service.ISessionService;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.repository.SessionRepository;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;
import ru.iteco.vetoshnikov.taskmanager.util.SqlConnect;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SessionService extends AbstractService implements ISessionService {
    private IServiceLocator serviceLocator;
    private SessionRepository sessionRepository;

    public SessionService(IServiceLocator serviceLocator, SessionRepository sessionRepository) {
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Override
    public User checkPassword(
            @Nullable final String userLogin,
            @Nullable final String userPassword
    ) {
        if (userLogin == null || userLogin.isEmpty()) return null;
        if (userPassword == null || userPassword.isEmpty()) return null;
        @Nullable final List<User> userList = serviceLocator.getUserService().findAll();
        for (User getUser : userList) {
            if (userLogin.equals(getUser.getLogin())) {
                @NotNull final User user = getUser;
                @Nullable final String passwordHash = HashUtil.getHash(userPassword);
                if (passwordHash == null) return null;
                if (passwordHash.equals(user.getPassword()))
                    return user;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Session createSession(
            @Nullable final String userId
    ) {
        if (userId == null || userId.isEmpty()) return null;
        Connection connection = null;
        Session session = null;
        try {
            connection = SqlConnect.getConnect();
            if (connection == null) return null;
            session = sessionRepository.createSession(connection, userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return session;
    }
}
