package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {
    @WebMethod
    Session getSession(
            @WebParam(name = "userLogin") @Nullable String userLogin,
            @WebParam(name = "userPass") @Nullable String userPass
    ) ;
}
