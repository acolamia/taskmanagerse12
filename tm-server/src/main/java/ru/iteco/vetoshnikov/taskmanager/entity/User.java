package ru.iteco.vetoshnikov.taskmanager.entity;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {
    @NotNull
    private String login=null;
    @NotNull
    private String password=null;
    @NotNull
    private String role = RoleType.USER.getDisplayName();

    public User(String login, String password, String role) {
        this.login = login;
        this.password = HashUtil.getHash(password);
        this.role = role;
    }
}
