package ru.iteco.vetoshnikov.taskmanager.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.vetoshnikov.taskmanager.entity.*;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;
import ru.iteco.vetoshnikov.taskmanager.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.IDomainEndpoint")
public class DomainEndpoint implements IDomainEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void loadBinary(@WebParam(name = "session") @Nullable final Session session) {
        SignatureUtil.check(session);
        try {
            if (isAdmin(session)) {
                @Nullable final File file = new File("dataBinary.bin");
                if (file.exists()) {
                    @NotNull final FileInputStream fileInputStream = new FileInputStream(file);
                    @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                    @Nullable final Domain domain = (Domain) objectInputStream.readObject();
                    objectInputStream.close();
                    fileInputStream.close();
                    loadDomain(domain);
                    System.out.println("Загрузка в базу завершена, необходимо перезайти в учетную запись.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @WebMethod
    public void saveBinary(@WebParam(name = "session") @Nullable final Session session) {
        SignatureUtil.check(session);
        try {
            if (isAdmin(session)) {
                @NotNull final Domain domain = saveDomain();
                @NotNull final File file = new File("dataBinary.bin");
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
                @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                objectOutputStream.writeObject(domain);
                objectOutputStream.close();
                fileOutputStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @WebMethod
    public void loadFasterJson(@WebParam(name = "session") @Nullable final Session session) {
        SignatureUtil.check(session);
        try {
            @NonNull final StringBuilder sb = new StringBuilder();
            @NonNull final BufferedReader reader = new BufferedReader(new FileReader("dataFasterJson.json"));
            while (true) {
                @Nullable final String line = reader.readLine();
                if (line == null) break;
                sb.append(line);
            }
            reader.close();
            @NonNull final String xml = sb.toString();
            if (xml.isEmpty()) {
                @NotNull final ObjectMapper mapper = new ObjectMapper();
                @Nullable final Domain domain = mapper.readValue(xml, Domain.class);
                loadDomain(domain);
                System.out.println("Загрузка в базу завершена, необходимо перезайти в учетную запись.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @WebMethod
    public void saveFasterJson(@WebParam(name = "session") @Nullable final Session session) {
        SignatureUtil.check(session);
        try {
            if (isAdmin(session)) {
                @NotNull final Domain domain = saveDomain();
                @NotNull final ObjectMapper mapper = new ObjectMapper();
                @NotNull final String result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
                FileWriter writer = new FileWriter("dataFasterJson.json");
                writer.write(result);
                writer.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @WebMethod
    public void loadFasterXml(@WebParam(name = "session") @Nullable final Session session) {
        SignatureUtil.check(session);
        try {
            if (isAdmin(session)) {
                @NonNull final StringBuilder sb = new StringBuilder();
                @NonNull final BufferedReader reader = new BufferedReader(new FileReader("fasterXml.xml"));
                while (true) {
                    @Nullable final String line = reader.readLine();
                    if (line == null) break;
                    sb.append(line);
                }
                reader.close();
                @NonNull final String xml = sb.toString();
                if (!xml.isEmpty()) {
                    @NotNull final ObjectMapper mapper = new XmlMapper();
                    @Nullable final Domain domain = mapper.readValue(xml, Domain.class);
                    loadDomain(domain);
                    System.out.println("Загрузка в базу завершена, необходимо перезайти в учетную запись.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @WebMethod
    public void saveFasterXml(@WebParam(name = "session") @Nullable final Session session) {
        SignatureUtil.check(session);
        try {
            if (isAdmin(session)) {
                @NotNull final Domain domain = saveDomain();
                @NotNull final ObjectMapper mapper = new XmlMapper();
                @NotNull final String result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
                FileWriter writer = new FileWriter("fasterXml.xml");
                writer.write(result);
                writer.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @WebMethod
    public void loadJaxbJson(@WebParam(name = "session") @Nullable final Session session) {
        SignatureUtil.check(session);
        try {
            if (isAdmin(session)) {
                final Map<String, Object> map = new HashMap<>();
                final Class[] classes = new Class[]{Domain.class};
                System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
                map.put("eclipselink.media-type", "application/json");
                @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(classes, map);
                @NotNull final Unmarshaller jaxbUnMarshaller = jaxbContext.createUnmarshaller();
                @Nullable final Domain domain = (Domain) jaxbUnMarshaller.unmarshal(new File("jaxbJson.json"));
                loadDomain(domain);
                System.out.println("Загрузка в базу завершена, необходимо перезайти в учетную запись.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @WebMethod
    public void saveJaxbJson(@WebParam(name = "session") @Nullable final Session session) {
        SignatureUtil.check(session);
        try {
            if (isAdmin(session)) {
                @NotNull final Domain domain = saveDomain();
                final Map<String, Object> map = new HashMap<>();
                final Class[] classes = new Class[]{Domain.class};
                System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
                map.put("eclipselink.media-type", "application/json");
                @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(classes, map);
                @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbMarshaller.marshal(domain, new File("jaxbJson.json"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @WebMethod
    public void loadJaxbXml(@WebParam(name = "session") @Nullable final Session session) {
        SignatureUtil.check(session);
        try {
            if (isAdmin(session)) {
                @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
                @NotNull final Unmarshaller jaxbUnMarshaller = jaxbContext.createUnmarshaller();
                @Nullable final Domain domain = (Domain) jaxbUnMarshaller.unmarshal(new File("jaxbXml.xml"));
                loadDomain(domain);
                System.out.println("Загрузка в базу завершена, необходимо перезайти в учетную запись.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @WebMethod
    public void saveJaxbXml(@WebParam(name = "session") @Nullable final Session session) {
        SignatureUtil.check(session);
        try {
            if (isAdmin(session)) {
                @NotNull final Domain domain = saveDomain();
                @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
                @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbMarshaller.marshal(domain, new File("jaxbXml.xml"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isAdmin(@Nullable final Session session) {
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        if (user == null) return false;
        if (!user.getRole().equals(RoleType.ADMINISTRATOR)) return false;
        return true;
    }

    private Domain saveDomain()  {
        final Domain domain = new Domain();
        @Nullable final List<User> userList = serviceLocator.getUserService().findAll();
        if (userList != null) domain.setUserList(userList);
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll();
        if (projectList != null) domain.setProjectList(projectList);
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll();
        if (taskList != null) domain.setTaskList(taskList);
        return domain;
    }

    private void loadDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getUserService().load(domain);
        serviceLocator.getProjectService().load(domain);
        serviceLocator.getTaskService().load(domain);
    }
}
